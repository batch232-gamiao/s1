package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        // variable

        int age;
        char middle_name;

        int x = 0;
        age = 18;

        // long
        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        // float
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char - can only hold 1 character
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        // constants in Java
        // final dataType VARIABLENAME
        // cannot be reassigned
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        // String - non-primitive data type. String are actually object that can use methods
        String username = "theTinker23";
        // isEmpty is a string method which returns a boolean
        // checks the length of the string, returns true if the length is 0, returns false otherwise
        System.out.println(username);
        System.out.println(username.isEmpty());
    }
}
